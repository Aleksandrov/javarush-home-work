package ru.rgtprojects.tests;

import com.javarush.test.level08.lesson08.task05.Solution;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by aleksandr on 24.01.17.
 */
public class test_080805 {
    public static void main(String[] args) {
        HashMap<String, String> map = Solution.createMap();
        HashSet<String> set = new HashSet<>();
        set.addAll(map.values());
        System.out.println("Map: " + map);
        System.out.println("Set: " + map.values());
        Solution.removeTheFirstNameDuplicates(map);
        System.out.println("Map: " + map);
    }
}
