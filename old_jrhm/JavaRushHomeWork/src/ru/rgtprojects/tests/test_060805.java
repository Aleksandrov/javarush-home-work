package ru.rgtprojects.tests;

import com.javarush.test.level06.lesson08.task05.StringHelper;

/**
 * Created by Aleksandr on 13.01.2017.
 * Тест к заданию JR level06.lesson08.task05
 */
public class test_060805 {
    public static void main(String[] args) {
        System.out.println(StringHelper.multiply("Амиго"));
        System.out.println(StringHelper.multiply("Амиго", 10));
    }
}
