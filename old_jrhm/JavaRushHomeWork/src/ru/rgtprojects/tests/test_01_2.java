package ru.rgtprojects.tests;

import java.util.Scanner;

/**
 * Created by aleksandr on 06.01.17.
 */
public class test_01_2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // обязательно использовать вызов данного метода!
        scanner.useDelimiter("\\n");

        while (true) {
            System.out.println("\nMenu Options\n");
            System.out.println("(1) - do this");
            System.out.println("(2) - quit");

            System.out.print("Please enter your selection:\t");
            int selection = scanner.nextInt();

            if (selection == 1) {
                System.out.print("Enter a sentence:\t");
                String sentence = scanner.next();

                System.out.print("Enter an index:\t");
                int index = scanner.nextInt();

                System.out.println("\nYour sentence:\t" + sentence);
                System.out.println("Your index:\t" + index);
            } else if (selection == 2) {
                break;
            }
        }
    }
}
