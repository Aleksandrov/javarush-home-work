package ru.rgtprojects.tests;

import com.javarush.test.level06.lesson08.task04.ConsoleReader;

/**
 * Created by Aleksandr on 13.01.2017.
 * Тест к заданию JR level06.lesson08.task04.ConsoleReader
 */
public class test_060804 {
    public static void main(String[] args) throws Exception {
        System.out.println("Чтение строки: ");
        System.out.println("Result = " + ConsoleReader.readString());
        System.out.println("==========");
        System.out.println("Чтение числа типа double: ");
        double d = ConsoleReader.readDouble();
        System.out.println("Result = " + d);
        System.out.println("==========");
        System.out.println("Чтение числа типа int: ");
        int i = ConsoleReader.readInt();
        System.out.println("Result = " + i);
        System.out.println("==========");
        System.out.println("Чтение числа типа boolean: ");
        boolean b = ConsoleReader.readBoolean();
        System.out.println("Result = " + b);
        System.out.println("==========");
    }
}
