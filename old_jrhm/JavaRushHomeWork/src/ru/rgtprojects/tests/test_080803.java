package ru.rgtprojects.tests;

import com.javarush.test.level08.lesson08.task03.Solution;

import java.util.HashMap;

/**
 * Created by aleksandr on 23.01.17.
 */
public class test_080803 {
    public static void main(String[] args) {
        HashMap<String, String> map  = Solution.createMap();
        System.out.println(map);
        System.out.println(Solution.getCountTheSameFirstName(map, "Иван"));
        System.out.println(Solution.getCountTheSameLastName(map, "Александров"));
    }
}
