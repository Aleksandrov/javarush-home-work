package ru.rgtprojects.tests;

import com.javarush.test.level06.lesson08.task03.Util;

/**
 * Created by Aleksandr on 13.01.2017.
 * Проверка задачи level06.lesson08.task03
 */
public class test_04_1 {
    public static void main(String[] args) {
        System.out.println(
            String.format("%s%s",
                "Расстояние между точками (3,3) и (1,2) = ",
                String.valueOf(Util.getDistance(3, 3, 1, 2))
                )
        );
    }
}
