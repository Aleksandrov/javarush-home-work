package ru.rgtprojects.tests;

/**
 * Created by mrResident on 07.01.17.
 */
public class test_02_1 {

    public static void main(String[] args) {
        StringCompare();
        System.out.println("===============");
        RealNumberCompare();
        System.out.println("===============");
        NanCompare();
        System.out.println("===============");
        OtherCompare();
        System.out.println("===============");
        AutoboxingTest();
    }

    private static void StringCompare() {
        System.out.println("1. Сравнение одинаковых строк объявленых как литералы");
        String str1 = "string";
        String str2 = "string";
        System.out.println(String.format(
            "%s = %s : %s",
            str1,
            str2,
            str1==str2 ? "the same" : "not the same")
        );
        str1 = "string";
        str2 = "str";
        String str3 = "ing";
        System.out.println(String.format(
            "%s = %s + %s : %s",
            str1,
            str2,
            str3,
            str1==str2 + str3 ? "the same" : "not the same")
        );
        System.out.println("1.2. Использования String.Intern");
        String str4 = "string";
        String str5 = new String("string");
        System.out.println(String.format(
            "%s = %s : %s",
            str4,
            str5,
            str4.intern()==str2.intern() ? "the same" : "not the same")
        );
    }

    private static void RealNumberCompare() {
        System.out.println("2. Сравнение вещественных чисел");
        System.out.println("2.1. Test 1");
        float f1 = 0.7f;
        float f2 = 0.3f + 0.4f;
        System.out.println("0.7f == 0.3f + 0.4f: " + (f1 == f2));

        System.out.println("2.2. Test 2");

        f1 = 0.3f;
        f2 = 0.4f;
        float f3 = f1 + f2;
        float f4 = 0.7f;
        System.out.println("f1="+(double)f1);
        System.out.println("f2="+(double)f2);
        System.out.println("f3="+(double)f3);
        System.out.println("f4="+(double)f4);

        System.out.println("2.2. Test 3");

        f1 = 0.3f;
        f2 = 0.4f;
        f3 = f1 + f2;
        f4 = 0.7f;
        System.out.println("|(0.3f + 0.4f) - 0.7f| < 1e-6: "
            + (Math.abs(f3-f4) < 1e-6));

        System.out.println("2.2. Test 4");

        f1 = 0.0f/1.0f;
        f2 = 0.0f/-1.0f;
        System.out.println("0.0f/1.0f = " + f1);
        System.out.println("-0.0f/1.0f = " + f2);
        System.out.println("0.0f/1.0f == -0.0f/1.0f: " + (f1 == f2));
        f3 = 1.0f / f1;
        f4 = 1.0f / f2;
        System.out.println("1.0f/(0.0f/1.0f) = " + f3);
        System.out.println("1.0f/(-0.0f/1.0f) = " + f4);

        System.out.println("2.2. Test 5");

        int i1 = Float.floatToIntBits(f1);
        int i2 = Float.floatToIntBits(f2);
        System.out.println("i1 (+0.0):"+ Integer.toBinaryString(i1));
        System.out.println("i2 (-0.0):"+ Integer.toBinaryString(i2));
        System.out.println("i1==i2: "+(i1 == i2));
    }

    private static void NanCompare() {
        System.out.println("3. Значение NaN");
        System.out.println("3.1 Test 1");

        float x = 0.0f/0.0f;
        System.out.println("0.0f / 0.0f = "+x);
        System.out.println("0.0f == 0.0f : " + (x == x));
    }

    private static void OtherCompare() {
        System.out.println("4. Прочее");
        System.out.println("4.1 Test 1");
        Integer i1 = new Integer(1);
        Integer i2 = new Integer(1);
        System.out.println("i1 = new Integer(1) == i2 = new Integer(1): "
            + (i1 == i2));
        System.out.println("4.1 Test 2");
        i1 = 1;
        i2 = 1;
        System.out.println("Integer i1 = 1 == i2 = 1 : "+(i1==i2));
    }

    private static void AutoboxingTest() {
        System.out.println("5. Autoboxing Test");
        int numbers[] = new int[]{-129,-128,127,128};

        for (int number : numbers) {
            Integer i1 = number;
            Integer i2 = number;
            System.out.println("number=" + number + ": " + (i1 == i2));
        }
    }
}
