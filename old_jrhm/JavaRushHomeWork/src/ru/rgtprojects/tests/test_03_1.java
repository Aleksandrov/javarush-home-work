package ru.rgtprojects.tests;

import com.javarush.test.level05.lesson05.task02.Cat;

/**
 * Created by Aleksandr on 09.01.2017.
 */
public class test_03_1 {

  public static void main(String[] args) {
    Cat cat1 = new Cat();
    cat1.name = "vaskya";
    cat1.age = 7;
    cat1.weight = 5;
    cat1.strength = 6;

    Cat cat2 = new Cat();
    cat2.name = "cat";
    cat2.age = 5;
    cat2.weight = 5;
    cat2.strength = 6;

    System.out.println(cat1.fight(cat2));
    System.out.println(cat2.fight(cat1));
  }

}
