package ru.rgtprojects.tests;

import com.javarush.test.level08.lesson08.task04.Solution;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by aleksandr on 23.01.17.
 */
public class test_080804 {
    public static void main(String[] args) {
        HashMap<String, Date> map = Solution.createMap();
        System.out.println(map);
        Solution.removeAllSummerPeople(map);
        System.out.println(map);
    }
}
