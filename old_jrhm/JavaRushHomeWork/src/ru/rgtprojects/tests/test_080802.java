package ru.rgtprojects.tests;

import com.javarush.test.level08.lesson08.task02.Solution;

import java.util.HashSet;

/**
 * Created by aleksandr on 23.01.17.
 */
public class test_080802 {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set = Solution.createSet();
        System.out.println(set);
        Solution.removeAllNumbersMoreThan10(set);
        System.out.println(set);
    }
}
