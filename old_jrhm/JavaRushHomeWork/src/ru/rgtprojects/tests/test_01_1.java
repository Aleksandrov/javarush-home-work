package ru.rgtprojects.tests;

import java.util.Scanner;

/**
 * Created by aleksandr on 06.01.17.
 */
public class test_01_1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");

        System.out.println("Enter an index:\n");
        int index = scanner.nextInt();

        System.out.println("Enter a sentence:\n");
        String sentence = scanner.next();

        System.out.println("\nYour sentence:\t" + sentence);
        System.out.println("Your index:\t" + index);
    }
}
