package ru.rgtprojects.tests;

/**
 * Created by aleksandr on 08.01.17.
 * Примеры из статьи Операторы перехода https://goo.gl/PAHSPF
 */
public class test_02_2 {

    public static void main(String[] args) {
        BreakTest();
        System.out.println("==============");
        BreakLoop();
        System.out.println("==============");
        ContinueTest();
        System.out.println("==============");
        ContinueLabel();
    }

    public static void BreakTest() {
        boolean t = true;
        first:
        {
            second:
            {
                third:
                {
                    System.out.println("Перед оператором break.");
                    if (t) {
                        break second; // выход из блока second
                    }
                    System.out.println("Данный оператор никогда не выполнится");
                }
                System.out.println("Данный оператор никогда не выполнится ");
            }
            System.out.println("Данный оператор размещен после блока second.");
        }
    }

    public static void BreakLoop() {
        outer:
        for (int i = 0; i < 3; i++) {
            System.out.print("Итерация " + i + ": ");
            for (int j = 0; j < 100; j++) {

                if (j == 10) {
                    break outer; // выйти из обоих циклов
                }
                System.out.print(j + " ");
            }
            System.out.println("Эта строка никогда не будет выведена");
        }
        System.out.println("Цикл завершен.");
    }

    public static void ContinueTest() {
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
            if (i % 2 == 0) {
                continue;
            }
            System.out.println("");
        }
    }

    public static void ContinueLabel() {
        outer:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j > i) {
                    System.out.println();
                    continue outer;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
    }
}
