package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

import java.util.Date;

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конструкторы
        //-
        String FIO;
        Date date_born;
        int sex;
        String address;
        int telNumber;
        int inn;

        public Human(String FIO) {
            this.FIO = FIO;
            date_born = null;
            sex = -1;
            address = "";
            telNumber = 0;
            inn = 0;
        }

        public Human(String FIO, Date date_born) {
            this.FIO = FIO;
            this.date_born = date_born;
            sex = 0;
            address = "";
            telNumber = 0;
            inn = 0;
        }

        public Human(String FIO, Date date_born, int sex) {
            this.FIO = FIO;
            this.date_born = date_born;
            this.sex = sex;
            address = "";
            telNumber = 0;
            inn = 0;
        }

        public Human(String FIO, Date date_born, int sex, String address) {
            this.FIO = FIO;
            this.date_born = date_born;
            this.sex = sex;
            this.address = address;
            telNumber = 0;
            inn = 0;
        }

        public Human(String FIO, Date date_born, int sex, String address, int telNumber) {
            this.FIO = FIO;
            this.date_born = date_born;
            this.sex = sex;
            this.address = address;
            this.telNumber = telNumber;
            inn = 0;
        }

        public Human(String FIO, Date date_born, int sex, String address, int telNumber, int inn) {
            this.FIO = FIO;
            this.date_born = date_born;
            this.sex = sex;
            this.address = address;
            this.telNumber = telNumber;
            this.inn = inn;
        }

        public Human(String FIO, int inn) {
            this.FIO = FIO;
            date_born = null;
            sex = -1;
            address = "";
            telNumber = 0;
            this.inn = inn;
        }

        public Human(String FIO, int telNumber, int inn) {
            this.FIO = FIO;
            date_born = null;
            sex = -1;
            address = "";
            this.telNumber = telNumber;
            this.inn = inn;
        }

        public Human(String FIO, String address, int telNumber, int inn) {
            this.FIO = FIO;
            date_born = null;
            sex = -1;
            this.address = address;
            this.telNumber = telNumber;
            this.inn = inn;
        }

        public Human(String FIO, String address) {
            this.FIO = FIO;
            date_born = null;
            sex = -1;
            this.address = address;
            telNumber = 0;
            inn = 0;
        }
        //-
    }
}
