package ru.resprojects.jrhw;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

@JsonAutoDetect
public class Cat1 {

    public String name;
    @JsonDeserialize(as = ArrayList.class, contentAs = Cat1.class)
    public List<Cat1> cats = new ArrayList<>();

    Cat1() {}

    @Override
    public String toString() {
        return "Cat1{" +
            "name='" + name + '\'' +
            ", cats=" + cats +
            '}';
    }
}
