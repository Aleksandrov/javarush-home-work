package ru.resprojects.jrhw;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.resprojects.jrhw.pets.Dog;
import ru.resprojects.jrhw.pets.House;
import ru.resprojects.jrhw.pets.Pet;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

public class JCollect3Lvl3Lect {

    public static void main(String[] args) throws IOException {
        System.out.println("JSON serialization");
        Cat cat = new Cat();
        cat.name = "Murka";
        cat.age = 5;
        cat.weight = 4;
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(writer, cat);
        String result = writer.toString();
        System.out.println(result);
        System.out.println("JSON deserialization");
        StringReader reader = new StringReader(result);
        ObjectMapper mapper1 = new ObjectMapper();
        Cat cat1 = mapper1.readValue(reader, Cat.class);
        System.out.println(cat1);
        System.out.println("=========");
        String jsonString = "{\"name\":\"Murka\",\"cats\":[{\"name\":\"Timka\"},{\"name\":\"Killer\"}]}";
        StringReader reader1 = new StringReader(jsonString);
        ObjectMapper mapper2 = new ObjectMapper();
        Cat1 cat11 = mapper2.readValue(reader1, Cat1.class);
        System.out.println(cat11);
        System.out.println("=========");
        ru.resprojects.jrhw.pets.Cat cat2 = new ru.resprojects.jrhw.pets.Cat();
        cat2.name = "Murka";
        cat2.age = 5;
        Dog dog = new Dog();
        dog.name = "Killer";
        dog.age = 8;
        dog.owner = "Bill Jeferson";
        House house = new House();
        house.pets.add(cat2);
        house.pets.add(dog);
        writer = new StringWriter();
        mapper = new ObjectMapper();
        mapper.writeValue(writer, house);
        System.out.println(writer.toString());
    }

}
