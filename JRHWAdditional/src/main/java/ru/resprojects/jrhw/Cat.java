package ru.resprojects.jrhw;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Cat {
    public String name;
    public int age;
    public int weight;

    public Cat() {
    }

    @Override
    public String toString() {
        return "Cat{" +
            "name='" + name + '\'' +
            ", age=" + age +
            ", weight=" + weight +
            '}';
    }
}
