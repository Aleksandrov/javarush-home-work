package ru.resprojects.jrhw;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class JC9Lvl7Lect {

    public static void main(String[] args) throws IOException {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outputStream);
        System.setOut(stream);
        printSomething();
        String result = outputStream.toString();
        System.setOut(consoleStream);
        StringBuilder sb = new StringBuilder(result);
        sb.reverse();
        System.out.println(sb);
    }

    private static void printSomething() {
        System.out.println("Hi");
        System.out.println("My name is Amigo!");
        System.out.println("Bye-Bye!");
    }

}
