package ru.resprojects.generics.test1;

public class BoxPrinter<T> {

    private T val;

    public BoxPrinter(T val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "BoxPrinter{" +
            "val=" + val +
            '}';
    }

    public T getVal() {
        return val;
    }
}
