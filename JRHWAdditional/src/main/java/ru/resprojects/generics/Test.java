package ru.resprojects.generics;

import ru.resprojects.generics.test1.BoxPrinter;
import ru.resprojects.generics.test1.Pair;
import ru.resprojects.generics.test1.Utilities;

import java.util.ArrayList;
import java.util.List;

public class Test {

    static void printList(List<?> list) {
        for (Object l : list) {
            System.out.println("{" + l + "}");
        }
    }

    public static void main(String[] args) {
        BoxPrinter<Integer> value1 = new BoxPrinter<>(10);
        System.out.println(value1);
        Integer intVal = value1.getVal();
        BoxPrinter<String> value2 = new BoxPrinter<>("Hello World!");
        System.out.println(value2);
        System.out.println("=========");
        Pair<Integer, String> pair = new Pair<>(6, "Data");
        System.out.println(pair);
        System.out.println("=========");
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        System.out.println("Список до обработки дженерик-методом: " + integers);
        Utilities.fill(integers, 0);
        System.out.println("Список после обработки дженерик-методом: " + integers);
        System.out.println("=========");
        integers = new ArrayList<>();
        integers.add(10);
        integers.add(100);
        printList(integers);
        List<String> strings = new ArrayList<>();
        strings.add("-10-");
        strings.add("-100-");
        printList(strings);
    }

}
