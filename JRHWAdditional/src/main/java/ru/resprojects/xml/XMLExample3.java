package ru.resprojects.xml;

//import ru.resprojects.xml.test2.Card;
//import ru.resprojects.xml.test2.CardList;
//import ru.resprojects.xml.test2.Cat;
//import ru.resprojects.xml.test2.Dog;
//import ru.resprojects.xml.test2.Zoo;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
//import javax.xml.bind.Unmarshaller;
//import java.io.StringReader;
//import java.io.StringWriter;
//import java.util.ArrayList;
//import java.util.List;
//
//public class XMLExample3 {
//
//    public static void main(String[] args) throws JAXBException {
//        String xmldata = "<zoo><cat/><cat/><dog/><cat/></zoo>";
//        Cat cat = new Cat();
//        cat.age = 1;
//        cat.name = "barsik";
//        cat.weight = 2;
//        Dog dog = new Dog();
//        dog.age = 5;
//        dog.name = "sharik";
//        dog.weight = 10;
//        Zoo zoo = new Zoo();
//        zoo.animals.add(cat);
//        zoo.animals.add(cat);
//        zoo.animals.add(dog);
//        zoo.animals.add(cat);
//        StringWriter writer = new StringWriter();
//        JAXBContext context = JAXBContext.newInstance(Cat.class, Zoo.class, Dog.class);
//        Marshaller marshaller = context.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        marshaller.marshal(zoo, writer);
//        System.out.println(writer);
//        StringReader reader = new StringReader(xmldata);
//        Unmarshaller unmarshaller = context.createUnmarshaller();
//        Zoo zoo1 = (Zoo) unmarshaller.unmarshal(reader);
//        for (Object object : zoo1.animals) {
//            System.out.println(object.getClass().getName());
//        }
//        CardList cards = new CardList();
//        cards.cards.add(Card.CLUBS);
//        cards.cards.add(Card.DIAMONDS);
//        cards.cards.add(Card.HEARTS);
//        cards.cards.add(Card.SPADES);
//        writer = new StringWriter();
//        JAXBContext context1 = JAXBContext.newInstance(Card.class, CardList.class);
//        marshaller = context1.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        marshaller.marshal(cards, writer);
//        System.out.println(writer);
//    }
//
//}
