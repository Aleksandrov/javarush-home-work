package ru.resprojects.xml;

//import ru.resprojects.xml.test1.Cat;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
//import javax.xml.bind.Unmarshaller;
//import java.io.StringReader;
//import java.io.StringWriter;
//
//public class XMLExample1 {
//
//    public static void main(String[] args) throws JAXBException {
//        Cat cat = new Cat();
//        cat.name = "Murka";
//        cat.age = 5;
//        cat.weight = 4;
//
//        StringWriter writer = new StringWriter();
//        JAXBContext context = JAXBContext.newInstance(Cat.class);
//        Marshaller marshaller = context.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        marshaller.marshal(cat, writer);
//        String result = writer.toString();
//        System.out.println(result);
//        System.out.println("===============");
//        String xmldata = "<cat><name>Murka</name><age>5</age><weight>4</weight></cat>";
//        StringReader reader = new StringReader(xmldata);
//        JAXBContext context1 = JAXBContext.newInstance(Cat.class);
//        Unmarshaller unmarshaller = context1.createUnmarshaller();
//        Cat cat1 = (Cat) unmarshaller.unmarshal(reader);
//        System.out.println(cat1);
//    }
//
//}
