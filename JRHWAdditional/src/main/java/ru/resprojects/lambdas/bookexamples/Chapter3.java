package ru.resprojects.lambdas.bookexamples;

import ru.resprojects.lambdas.bookexamples.datamodel.Album;
import ru.resprojects.lambdas.bookexamples.datamodel.Artist;
import ru.resprojects.lambdas.bookexamples.datamodel.SampleData;
import ru.resprojects.lambdas.bookexamples.datamodel.Track;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Chapter3 {

    public static void main(String[] args) {
        Album album = SampleData.aLoveSupreme;
        Set<String> origins = album.getMusicians()
            .filter(artist -> artist.getName().startsWith("The"))
            .map(Artist::getNationality)
            .collect(Collectors.toSet());
        System.out.println(origins);
        System.out.println(findLongTracks(Stream.of(
            SampleData.manyTrackAlbum,
            SampleData.sampleShortAlbum,
            SampleData.aLoveSupreme
            ).collect(Collectors.toList()))
        );
        System.out.println(addUpp(Stream.of(1, 2, 3, 4, 5)));
        List<String> list  = SampleData.membersOfTheBeatles.stream()
            .map((artist -> "name: " + artist.getName() + " nationality: " + artist.getNationality()))
            .collect(Collectors.toList());
        System.out.println(list);
        List<Album> albums = Stream.of(SampleData.aLoveSupreme, SampleData.manyTrackAlbum, SampleData.sampleShortAlbum)
            .filter(tracks -> tracks.getTrackList().size() <= 3 && tracks.getTrackList().size() > 0)
            .collect(Collectors.toList());
        albums.forEach(x -> System.out.println("Album name: " + x.getName() + "; tracks count in album = " + x.getTrackList().size()));
        int totalMembers = 0;
        for (Artist artist : SampleData.getThreeArtists()) {
            Stream<Artist> members = artist.getMembers();
            totalMembers += members.count();
        }
        System.out.println(totalMembers);
        System.out.println("Answer 2: " + SampleData.threeArtists()
            .map(x -> x.getMembers().count())
            .reduce((long) 0, (acc, x) -> acc + x));
        System.out.println("Answer 6: " + "Hello world".chars()
            .filter(ch -> IntStream.range('a', 'z').anyMatch(x -> x == ch))
            .count());
        List<String> stringList = new ArrayList<>();
        stringList.add("abc");
        stringList.add("abcde");
        stringList.add("ab");
        System.out.println(stringList.stream().max(Comparator.comparing(Chapter3::countLowercaseLetters)));
    }

    public static Set<String> findLongTracks(List<Album> albums) {
        return albums.stream()
            .flatMap(Album::getTracks)
            .filter(track -> track.getLength() > 60)
            .map(Track::getName)
            .collect(Collectors.toSet());
    }

    public static int addUpp(Stream<Integer> numbers) {
        return numbers.reduce(0, (acc, number) -> acc + number);
    }

    public static int countLowercaseLetters(String string) {
        return (int) string.chars()
            .filter(Character::isLowerCase)
            .count();
    }

}
