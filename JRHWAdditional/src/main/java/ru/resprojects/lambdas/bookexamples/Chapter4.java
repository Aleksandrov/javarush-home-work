package ru.resprojects.lambdas.bookexamples;

import ru.resprojects.lambdas.bookexamples.datamodel.Album;
import ru.resprojects.lambdas.bookexamples.datamodel.SampleData;
import ru.resprojects.lambdas.bookexamples.datamodel.Track;

import java.util.IntSummaryStatistics;

public class Chapter4 {

    public static void main(String[] args) {
        printTrackLengthStatistics(SampleData.manyTrackAlbum);
    }

    public static void printTrackLengthStatistics(Album album) {
        IntSummaryStatistics trackLengthStats = album.getTracks()
            .mapToInt(Track::getLength)
            .summaryStatistics();
        System.out.printf(
            "Max: %d, Min: %d, Ave: %f, Sum: %d",
            trackLengthStats.getMax(),
            trackLengthStats.getMin(),
            trackLengthStats.getAverage(),
            trackLengthStats.getSum()
        );
    }

}
