package ru.resprojects.lambdas.bookexamples;

import ru.resprojects.lambdas.bookexamples.datamodel.Artist;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class Chapter5 {

    public static void main(String[] args) {
        BiFunction<String, String, Artist> biFunction = Artist::new;
        Artist artist = biFunction.apply("artis", "national");
        System.out.println(artist.getName() + " " + artist.getNationality());
        System.out.println("========");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

    }

}
