package ru.resprojects.lambdas;

import javax.swing.text.DateFormatter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LambdaTest1 {

    public static void main(String[] args) {
        ThreadLocal<DateFormatter> formatter = ThreadLocal.withInitial(
            () -> new DateFormatter(new SimpleDateFormat("dd-MM-yyyy")));
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        long count = list.stream().filter((integer) -> {
            System.out.println("from lambda = " + integer);
            return integer % 2 == 0;
        }).count();
        System.out.println(count);
        List<String> list1 = Stream.of("a", "b", "c").map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(list1);
        List<String> list2 = Stream.of("a", "1abc", "abc1")
            .filter(value -> Character.isDigit(value.charAt(0)))
            .collect(Collectors.toList());
        System.out.println(list2);
        List<Integer> together = Stream.of(Arrays.asList(1, 2), Arrays.asList(3, 4))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
        System.out.println(together);
        Integer maxfig = Stream.of(1, 3, 9, 5, 4, 6, 7, 8, 0)
            .max(Comparator.comparing(value -> value))
            .get();
        System.out.println(maxfig);
        Integer minfig = Stream.of(9, 2, 4, 3, 1, 6, 7, 8, 5)
            .min(Comparator.comparing(value -> value))
            .get();
        System.out.println(minfig);
        long count1 = Stream.of("a", "b", "c").count();
        System.out.println(count1);
        int sum = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
            .reduce(0, (acc, element) -> acc + element);
        System.out.println(sum);
    }

}
