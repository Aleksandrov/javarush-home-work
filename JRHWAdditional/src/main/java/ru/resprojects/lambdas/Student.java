package ru.resprojects.lambdas;

import java.util.Objects;

public class Student {

    private String name;
    private Speciality speciality;
    private int grade;

    public Student(String name, Speciality speciality, int grade) {
        this.name = name;
        this.speciality = speciality;
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return grade == student.grade &&
            Objects.equals(name, student.name) &&
            speciality == student.speciality;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, speciality, grade);
    }

    @Override
    public String toString() {
        return "Student{" +
            "name='" + name + '\'' +
            ", speciality=" + speciality +
            ", grade=" + grade +
            '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
