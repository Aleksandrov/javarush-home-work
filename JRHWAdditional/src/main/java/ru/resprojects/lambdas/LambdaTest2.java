package ru.resprojects.lambdas;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class LambdaTest2 {

    public static void main(String[] args) {
        String[] strings = new String[5];
        strings[0] = "123";
        strings[1] = "123dfd";
        strings[2] = "12";
        strings[3] = "fsdfe";
        strings[4] = "fe3";
        List<String> stringList = Arrays.stream(strings)
            .filter(s -> s.length() <= 3)
            .collect(Collectors.toList());
        System.out.println(stringList);
        System.out.println("==============");
        IntStream.of(120, 410, 85, 32, 314, 12)
            .filter(x -> x < 300)
            .map(x -> x + 11)
            .limit(3)
            .forEach(System.out::println);
        System.out.println("==============");
        System.out.println(IntStream.range(0, 10)
            .parallel()
            .map(x -> x * 2)
            .sum()
        );
        System.out.println("==============");
        Stream.of(1, 2, 3).forEach(System.out::println);
        System.out.println("==============");
        Stream.generate(Math::random)
            .limit(6)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.iterate(2, x -> x + 6)
            .limit(6)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.iterate(1, x -> x * 2)
            .limit(6)
            .forEach(System.out::println);
        System.out.println("==============");
        String str = Math.random() > 0.5 ? "I'm feeling lucky" : null;
        Stream.ofNullable(str)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.iterate(2, x -> x < 25, x -> x + 6)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.concat(
            Stream.of(1, 2, 3),
            Stream.of(4, 5, 6))
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.Builder<Integer> streamBuilder = Stream.<Integer>builder()
            .add(0)
            .add(1);
        for (int i = 2; i <= 8; i++) {
            streamBuilder.accept(i);
        }
        streamBuilder
            .add(9)
            .add(10)
            .build()
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of("3", "4", "5")
            .map(Integer::parseInt)
            .map(x -> x + 10)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(2, 3, 0, 1, 3)
            .flatMapToInt(x -> IntStream.range(0, x))
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(1, 2 , 3, 4, 5, 6)
            .flatMap(x -> {
                switch (x % 3) {
                    case 0:
                        return Stream.of(x, x * x, x * x * x);
                    case 1:
                        return Stream.of(0);
                    case 2:
                        default:
                            return Stream.empty();
                }
            })
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(1, 2 , 3, 4, 5, 6)
            .limit(4)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(1, 2 , 3, 4, 5, 6)
            .skip(3)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(120, 410, 85, 32, 314, 12)
            .sorted()
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(120, 410, 85, 32, 314, 12)
            .sorted(Comparator.reverseOrder())
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(2, 1, 8, 1, 3, 2)
            .distinct()
            .forEach(System.out::println);
        System.out.println("==============");
        IntStream.concat(
            IntStream.range(2, 5),
            IntStream.range(0, 4))
            .distinct()
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(0, 3, 0, 0, 5)
            .peek(x -> System.out.format("before distinct: %d%n", x))
            .distinct()
            .peek(x -> System.out.format("after distinct: %d%n", x))
            .map(x -> x * x)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(1, 2, 3, 4, 2, 5)
            .takeWhile( x -> x < 3)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(1, 2, 3, 4, 2, 5)
            .dropWhile(x -> x < 3)
            .forEach(System.out::println);
        System.out.println("==============");
        DoubleStream.of(0.1, Math.PI)
            .boxed()
            .map(Object::getClass)
            .forEach(System.out::println);
        System.out.println("==============");
        Stream.of(120, 410, 85, 32, 314, 12)
            .forEach(x -> System.out.format("%s, ", x));
        System.out.println();
        System.out.println("==============");
        IntStream.range(0, 100000)
            .parallel()
            .filter(x -> x % 10000 == 0)
            .map(x -> x /10000)
            .forEach(System.out::println);
        System.out.println("==============");
        IntStream.range(0, 100000)
            .parallel()
            .filter(x -> x % 10000 == 0)
            .map(x -> x /10000)
            .forEachOrdered(System.out::println);
        System.out.println("==============");
        long count = IntStream.range(0, 10)
            .flatMap(x -> IntStream.range(0, x))
            .count();
        System.out.println(count);
        System.out.println("==============");
        List<Integer> list = Stream.of(1, 2, 3)
            .collect(Collectors.toList());
        System.out.println(list);
        System.out.println("==============");
        String s = Stream.of(1, 2, 3)
            .map(String::valueOf)
            .collect(Collectors.joining("-", "<", ">"));
        System.out.println(s);
        System.out.println("==============");
        List<String> list1 = Stream.of("a", "b", "c", "d")
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        System.out.println(list1);
        System.out.println("==============");
        int sum = Stream.of(1, 2, 3, 4, 5)
            .reduce(10, (acc, x) -> acc + x);
        System.out.println(sum);
        System.out.println("==============");
        Optional<Integer> sum1 = Stream.of(1, 2, 3, 4, 5)
            .reduce((acc, x) -> acc + x);
        System.out.println(sum1.get());
        System.out.println("==============");
        int product = IntStream.range(0, 10)
            .filter(x -> x++ % 4 == 0)
            .peek(x -> System.out.format("after filter: %d%n", x))
            .reduce((acc, x) -> acc + x)
            .getAsInt();
        System.out.println(product);
        System.out.println("==============");
        int min = Stream.of(20, 11, 45, 78, 13)
            .min(Integer::compare)
            .get();
        int max = Stream.of(20, 11, 45, 78, 13)
            .max(Integer::compare)
            .get();
        System.out.println("min = " + min + "; max = " + max);
        System.out.println("==============");
        int anySeq = IntStream.range(4, 65536)
            .parallel()
            .findAny()
            .getAsInt();
        System.out.println("anySeq = " + anySeq);
        System.out.println("==============");
        boolean result = Stream.of(1, 2, 3, 4, 5)
            .allMatch(x -> x < 7);
        System.out.println("result = " + result);
        System.out.println("==============");
        result = Stream.of(1, 2, 3, 4, 5)
            .anyMatch(x -> x == 3);
        System.out.println("result = " + result);
        System.out.println("==============");
        result = Stream.of(1, 2, 3, 4, 5)
            .noneMatch(x -> x == 6);
        System.out.println("result = " + result);
        System.out.println("==============");
        double avarage = IntStream.range(2, 16)
            .average()
            .getAsDouble();
        System.out.println(avarage);
        System.out.println("==============");
        long lresult = LongStream.range(2, 16)
            .sum();
        System.out.println(lresult);
        System.out.println("==============");
        LongSummaryStatistics stats = LongStream.range(2, 16)
            .summaryStatistics();
        System.out.format("  count: %d%n", stats.getCount());
        System.out.format("    sum: %d%n", stats.getSum());
        System.out.format("average: %.1f%n", stats.getAverage());
        System.out.format("    min: %d%n", stats.getMin());
        System.out.format("    max: %d%n", stats.getMax());
        System.out.println("==============");
        Set<Integer> set = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(Collectors.toCollection(LinkedHashSet::new));
        System.out.println(set);
        System.out.println("==============");
        Map<Integer, Integer> map1 = Stream.of(1, 2, 3, 4, 5)
            .collect(
                Collectors.toMap(
                    Function.identity(),
                    Function.identity()
                )
            );
        System.out.println(map1);
        System.out.println("==============");
        Map<Integer, String> map2 = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(
                Collectors.toMap(
                    Function.identity(),
                    i -> String.format("%d * 2 = %d", i, i * 2)
                )
            );
        System.out.println(map2);
        System.out.println("==============");
        Map<Character, String> map3 = Stream.of(50, 54, 55)
            .collect(
                Collectors.toMap(
                    i -> (char) i.intValue(),
                    i -> String.format("<%d>", i)
                )
            );
        System.out.println(map3);
        System.out.println("==============");
        Map<Integer, String> map4 = Stream.of(50, 55, 69, 20, 19, 52)
            .collect(
                Collectors.toMap(
                    i -> i % 5,
                    i -> String.format("<%d>", i),
                    (a, b) -> String.join(", ", a, b)
                )
            );
        System.out.println(map4);
        System.out.println("==============");
        Map<Integer, String> map5 = Stream.of(50, 55, 69, 20, 19, 52)
            .collect(
                Collectors.toMap(
                    i -> i % 5,
                    i -> String.format("<%d>", i),
                    (a, b) -> String.join(", ", a, b),
                    LinkedHashMap::new
                )
            );
        System.out.println(map5);
        System.out.println("==============");
        List<Integer> list2 = Stream.of(1, 2, 3, 4, 5)
            .collect(Collectors.collectingAndThen(
                Collectors.toList(),
                Collections::unmodifiableList)
            );
        System.out.println(list2.getClass());
        System.out.println("==============");
        List<String> list3 = Stream.of("a", "b", "c", "d")
            .collect(Collectors.collectingAndThen(
                Collectors.toMap(Function.identity(), s1 -> s1 + s1),
                map -> map.entrySet().stream()))
            .map(Object::toString)
            .collect(Collectors.collectingAndThen(
                Collectors.toList(),
                Collections::unmodifiableList));
        list3.forEach(System.out::println);
        System.out.println("==============");
        String s1 = Stream.of("a", "b", "c", "d")
            .collect(Collectors.joining());
        System.out.println(s1);
        String s2 = Stream.of("a", "b", "c", "d")
            .collect(Collectors.joining("-"));
        System.out.println(s2);
        String s3 = Stream.of("a", "b", "c", "d")
            .collect(Collectors.joining("->", "[", "]"));
        System.out.println(s3);
        System.out.println("==============");
        Integer sum2 = Stream.of("1", "2", "3", "4")
            .collect(Collectors.summingInt(Integer::parseInt));
        System.out.println(sum2);
        Double average = Stream.of("1", "2", "3", "4")
            .collect(Collectors.averagingInt(Integer::parseInt));
        System.out.println(average);
        DoubleSummaryStatistics stats1 = Stream.of("1.1", "2.34", "3.14", "4.04")
            .collect(Collectors.summarizingDouble(Double::parseDouble));
        System.out.println(stats1);
        System.out.println("==============");
        Long count1 = Stream.of("1", "2", "3", "4")
            .collect(Collectors.counting());
        System.out.println(count1);
        System.out.println("==============");
        List<Integer> ints = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(Collectors.filtering(
                x -> x % 2 == 0,
                Collectors.toList()));
        System.out.println(ints);
        s1 = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(Collectors.filtering(
                x -> x % 2 == 0,
                Collectors.mapping(
                    x -> Integer.toString(x),
                    Collectors.joining("-")
                )
            ));
        System.out.println(s1);
        s2 = Stream.of(2, 0, 1, 3, 2)
            .collect(Collectors.flatMapping(
                x -> IntStream.range(0, x).mapToObj(Integer::toString),
                Collectors.joining(", ")
            ));
        System.out.println(s2);
        int value = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(Collectors.reducing(
                0, (a, b) -> a + b
            ));
        System.out.println(value);
        s3 = Stream.of(1, 2, 3, 4, 5, 6)
            .collect(Collectors.reducing(
                "", x -> Integer.toString(x), (a, b) -> a + b
            ));
        System.out.println(s3);
        System.out.println("==============");
        Optional<String> min1 = Stream.of("ab", "c", "defgh", "ijk", "l")
            .collect(Collectors.minBy(Comparator.comparing(String::length)));
        min1.ifPresent(System.out::println);
        Optional<String> max1 = Stream.of("ab", "c", "defgh", "ijk", "l")
            .collect(Collectors.maxBy(Comparator.comparing(String::length)));
        max1.ifPresent(System.out::println);
        System.out.println("==============");
        Map<Integer, List<String>> map6 = Stream.of(
            "ab", "c", "def", "gh", "ijk", "l", "mnop")
            .collect(Collectors.groupingBy(String::length));
        map6.entrySet().forEach(System.out::println);
        System.out.println("-----");
        Map<Integer, String> map7 = Stream.of(
            "ab", "c", "def", "gh", "ijk", "l", "mnop")
            .collect(Collectors.groupingBy(
                String::length,
                Collectors.mapping(
                    String::toUpperCase,
                    Collectors.joining())
            ));
        map7.entrySet().forEach(System.out::println);
        System.out.println("-----");
        Map<Integer, List<String>> map8 = Stream.of(
            "ab", "c", "def", "gh", "ijk", "l", "mnop")
            .collect(Collectors.groupingBy(
                String::length,
                LinkedHashMap::new,
                Collectors.mapping(
                    String::toUpperCase,
                    Collectors.toList())
            ));
        map8.entrySet().forEach(System.out::println);
        System.out.println("==============");
        Map<Boolean, List<String>> map9 = Stream.of(
            "ab", "c", "def", "gh", "ijk", "l", "mnop")
            .collect(Collectors.partitioningBy(ss1 -> ss1.length() <= 2));
        map9.entrySet().forEach(System.out::println);
        System.out.println("-----");
        Map<Boolean, String> map10 = Stream.of(
            "ab", "c", "def", "gh", "ijk", "l", "mnop")
            .collect(Collectors.partitioningBy(
                ss2 -> ss2.length() <= 2,
                Collectors.mapping(
                    String::toUpperCase,
                    Collectors.joining("-"))
            ));
        map10.entrySet().forEach(System.out::println);
        System.out.println("==============");
        ArrayList<Integer> list4 = Stream.of(1, 2, 3, 1, 9, 2, 5, 3, 4, 8, 2)
            .collect(
                Collectors.collectingAndThen(
                    Collectors.toCollection(LinkedHashSet::new), ArrayList::new
                )
            );
        System.out.println(list4);
        System.out.println("==============");
        long count2 = StreamSupport
            .stream(Arrays.asList(0, 1, 2, 3).spliterator(), true)
            .count();
        System.out.println(count2);
        System.out.println("==============");
        StreamSupport.stream(new FibonacciSpliterator(7), true).forEach(System.out::println);
        System.out.println("==============");
        StreamSupport.stream(
            Spliterators.spliteratorUnknownSize(
                new FibonacciIterator(), Spliterator.ORDERED | Spliterator.SORTED), false)
            .limit(10)
            .forEach(System.out::println);
        System.out.println("==============");
        fibonacciStream().limit(15).forEach(System.out::println);
        System.out.println("==============");
        String[] arguments = {"-i", "in.txt", "--limit", "40", "-d", "1", "-o", "out.txt"};
        Map<String, String> argsMap = new LinkedHashMap<>(arguments.length / 2);
        for (int i = 0; i < arguments.length; i += 2) {
            argsMap.put(arguments[i], arguments[i + 1]);
        }
        argsMap.forEach((key, val) -> System.out.format("%s: %s%n", key, val));
        String[] args1 = argsMap.entrySet().stream()
            .flatMap(e -> Stream.of(e.getKey(), e.getValue()))
            .toArray(String[]::new);
        System.out.println(String.join(" ", args1));
        System.out.println("==============");
        List<Student> students = Arrays.asList(
            new Student("Alex", Speciality.Physics, 1),
            new Student("Rika", Speciality.Biology, 4),
            new Student("Julia", Speciality.Biology, 2),
            new Student("Steve", Speciality.History, 4),
            new Student("Mike", Speciality.Finance, 1),
            new Student("Hinata", Speciality.Biology, 2),
            new Student("Richard", Speciality.History, 1),
            new Student("Kate", Speciality.Psychology, 2),
            new Student("Sergey", Speciality.ComputerScience, 4),
            new Student("Maximilian", Speciality.ComputerScience, 3),
            new Student("Tim", Speciality.ComputerScience, 5),
            new Student("Ann", Speciality.Psychology, 1)
        );
        students.stream().collect(Collectors.groupingBy(Student::getGrade))
            .entrySet().forEach(System.out::println);
        System.out.println("-------------");
        students.stream()
            .map(Student::getSpeciality)
            .distinct()
            .sorted(Comparator.comparing(Enum::name))
            .forEach(System.out::println);
        System.out.println("-------------");
        students.stream()
            .collect(Collectors.groupingBy(
                Student::getSpeciality, Collectors.counting()))
            .forEach((s4, count4) -> System.out.println(s4 + ": " + count4));
        Map<Speciality, Map<Integer, List<Student>>> result4 = students.stream()
            .sorted(Comparator
            .comparing(Student::getSpeciality, Comparator.comparing(Enum::name))
            .thenComparing(Student::getGrade)
            )
            .collect(Collectors.groupingBy(
                Student::getSpeciality,
                LinkedHashMap::new,
                Collectors.groupingBy(Student::getGrade)));
        for (Map.Entry<Speciality, Map<Integer, List<Student>>> e : result4.entrySet()) {
            System.out.println(e.getKey());
            for (Map.Entry<Integer, List<Student>> e1 : e.getValue().entrySet()) {
                System.out.println("\t Grade: " + e1.getKey() + "; Students: " + e1.getValue());
            }
        }
        System.out.println("->");
        result4.forEach((s5, map) -> {
            System.out.println("-=" + s5 + " =-");
            map.forEach((year, list5) -> System.out.format(
                "%d: %s%n",
                year,
                list5.stream()
                .map(Student::getName)
                .sorted()
                .collect(Collectors.joining(", ")))
            );
        });
        System.out.println("-------------");
        System.out.println(students.stream()
            .filter(s6 -> !EnumSet.of(Speciality.ComputerScience, Speciality.Physics)
                .contains(s6.getSpeciality()))
            .anyMatch(s6 -> s6.getGrade() == 3));
        System.out.println("-------------");
        final Random rnd = new Random();
        final double r = 1000.0;
        final int max2 = 10000000;
        long count4 = IntStream.range(0, max2)
            .mapToObj(i11 -> rnd.doubles(2).map(x11 -> x11 * r).toArray())
            .parallel()
            .filter(arr11 -> Math.hypot(arr11[0], arr11[1]) <= r)
            .count();
        System.out.println(4.0 * count4 / max2);
        System.out.println("-------------");
        IntStream.rangeClosed(2, 9)
            .boxed()
            .flatMap(i -> IntStream.rangeClosed(2, 9)
                .mapToObj(j -> String.format("%d * %d = %d", i, j, i * j))
            )
            .forEach(System.out::println);
        System.out.println("-------------");
        IntFunction<IntFunction<String>> function = i -> j -> String.format("%d x %2d = %2d", i, j, i * j);
        IntFunction<IntFunction<IntFunction<String>>> repeaterX = count22 -> i -> j ->
            IntStream.range(0, count22)
                .mapToObj(delta -> function.apply(i + delta).apply(j))
                .collect(Collectors.joining("\t"));
        IntFunction<IntFunction<IntFunction<IntFunction<String>>>> repeaterY = countY -> countX -> i -> j ->
            IntStream.range(0, countY)
                .mapToObj(deltaY -> repeaterX.apply(countX).apply(i).apply(j + deltaY))
                .collect(Collectors.joining("\n"));
        IntFunction<String> row = i -> repeaterY.apply(10).apply(4).apply(i).apply(1) + "\n";
        IntStream.of(2, 6).mapToObj(row).forEach(System.out::println);
    }

    public static class FibonacciIterator implements Iterator<BigInteger> {

        private BigInteger a = BigInteger.ZERO;
        private BigInteger b = BigInteger.ONE;

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public BigInteger next() {
            BigInteger result = a;
            a = b;
            b = result.add(b);
            return result;
        }
    }

    public static Stream fibonacciStream() {
        return Stream.iterate(
            new BigInteger[] {BigInteger.ZERO, BigInteger.ONE},
            t -> new BigInteger[] {t[1], t[0].add(t[1])})
            .map(t -> t[0]);
    }

}
