package ru.resprojects.network.sockets.simpleserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        int serverPort = 6666;
        String address = "127.0.0.1";
        try {
            InetAddress ipAddress = InetAddress.getByName(address);
            System.out.println("Any of you heard of a socket wit IP address + "
                + address + " and port " + serverPort + "?");
            Socket socket = new Socket(ipAddress, serverPort);
            System.out.println("Yes! I just hold of the program.");
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();
            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;
            System.out.println("Type something and press enter. Will send it to the server and tell you what it thinks.");
            System.out.println();
            while (true) {
                line = keyboard.readLine();
                if ("exit".equalsIgnoreCase(line)) {
                    System.out.println("Sending last message to the server and terminate program.");
                    out.writeUTF(line);
                    break;
                }
                System.out.println("Sending this line to the server...");
                out.writeUTF(line);
                line = in.readUTF();
                System.out.println("The server was very polite. It send me this : " + line);
                System.out.println("Looks like the server is pleased with us. Go ahead and enter more lines.");
                System.out.println();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
