package ru.resprojects.thinkingjava.systemio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Logon implements Serializable {
    private Date date = new Date();
    private String username;
    private transient String password;

    public Logon(String name, String pwd) {
        username = name;
        password = pwd;
    }

    @Override
    public String toString() {
        return "logon info: \n   username: " + username
            + "\n   date: " + date + "\n   password: " + password;
    }

    public static void main(String[] args) throws Exception {
        Logon logon = new Logon("user", "123");
        System.out.println("logon a = " + logon);
        ObjectOutputStream out = new ObjectOutputStream(
            new FileOutputStream("logon.out"));
        out.writeObject(logon);
        out.close();
        TimeUnit.SECONDS.sleep(1);
        ObjectInputStream in = new ObjectInputStream(
            new FileInputStream("logon.out"));
        System.out.println("Recovering object at " + new Date());
        logon = (Logon) in.readObject();
        System.out.println("Logon = " + logon);
    }

}
