package ru.resprojects.thinkingjava.systemio;

/*
Поиск данных внутри файла
*/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WriteReadDataStructure {
    public static void main(String[] args) {
        if (args.length == 0) {
            return;
        }
        try {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in)
            );
            String fileName = reader.readLine();
            reader.close();
            DataOutputStream out = new DataOutputStream(
                new BufferedOutputStream(
                    new FileOutputStream(fileName)
                )
            );
            List<GoodsDataStorage> dataStorages = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                dataStorages.add(new GoodsDataStorage(
                    i,
                    "Good #" + String.valueOf(i),
                    new Random().nextDouble(),
                    new Random().nextInt(100))
                );
            }
            for (GoodsDataStorage dataStorage : dataStorages) {
                out.writeInt(dataStorage.getId());
                out.writeUTF(dataStorage.getProductName());
                out.writeDouble(dataStorage.getPrice());
                out.writeInt(dataStorage.getQuantity());
                out.writeChar('\n');
            }
            out.close();
            DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                    new FileInputStream(fileName)
                )
            );
            dataStorages.clear();
            while (in.available() > 0) {
                int id = in.readInt();
                String productName = in.readUTF();
                double price = in.readDouble();
                int quantity = in.readInt();
                in.readChar();
                dataStorages.add(new GoodsDataStorage(
                    id,
                    productName,
                    price,
                    quantity)
                );
            }
            for (GoodsDataStorage dataStorage : dataStorages) {
                if (dataStorage.getId() == Integer.valueOf(args[0])) {
                    System.out.println(String.format(
                        "%d %s %f %d",
                        dataStorage.getId(),
                        dataStorage.getProductName(),
                        dataStorage.getPrice(),
                        dataStorage.getQuantity()
                    ));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final class GoodsDataStorage {
        private int id;
        private String productName;
        private double price;
        private int quantity;

        public GoodsDataStorage(final int id, final String productName,
            final double price, final int quantity) {
            this.id = id;
            this.productName = productName;
            this.price = price;
            this.quantity = quantity;
        }

        public int getId() {
            return id;
        }

        public String getProductName() {
            return productName;
        }

        public double getPrice() {
            return price;
        }

        public int getQuantity() {
            return quantity;
        }
    }
}