package ru.resprojects.backpack;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Backpack {

    private List<Item> bestItems = null;
    private double maxW;
    private double bestPrice;

    public static void main(String[] args) {
        List<Item> items = new LinkedList<>();
        items.add(new Item("book", 1, 600));
        items.add(new Item("binoculars", 2, 5000));
        items.add(new Item("medkit", 4, 1500));
        items.add(new Item("notebook", 2, 40000));
        items.add(new Item("cauldron", 1, 500));
        Backpack backpack = new Backpack(6);
        backpack.MakeAllSets(items);
        System.out.println(backpack);
    }

    public Backpack(double maxW) {
        this.maxW = maxW;
    }

    private double CalcWeight(List<Item> items) {
        return items.stream()
            .map(Item::getWeight)
            .reduce(0.0,  (acc, weight) -> acc + weight);
    }

    private double CalcPrice(List<Item> items) {
        return items.stream()
            .map(Item::getPrice)
            .reduce(0.0, (acc, price) -> acc + price);
    }

    private void CheckSet(List<Item> items) {
        if (bestItems == null) {
            if (CalcWeight(items) <= maxW) {
                bestItems = items;
                bestPrice = CalcPrice(items);
            }
        } else {
            if (CalcWeight(items) <= maxW && CalcPrice(items) > bestPrice) {
                bestItems = items;
                bestPrice = CalcPrice(items);
            }
        }
    }

    public void MakeAllSets(List<Item> items) {
        if (items.size() > 0) {
            CheckSet(items);
        }

        for (int i = 0; i < items.size(); i++) {
            List<Item> newSet = new LinkedList<>(items);
            newSet.remove(i);
            MakeAllSets(newSet);
        }
    }

    public List<Item> getBestItems() {
        return bestItems;
    }

    @Override
    public String toString() {
        return "Backpack{" +
            "\nbestItems: [\n" + bestItems.stream().map(
                item -> "name = " + item.getName() + " weight = " + item.getWeight() + " price = " + item.getPrice()).collect(Collectors.joining(";\n")) + "] \n" +
            ", maxW=" + maxW + "\n" +
            ", bestPrice=" + bestPrice + "\n" +
            '}';
    }
}
