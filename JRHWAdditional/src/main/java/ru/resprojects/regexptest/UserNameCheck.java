package ru.resprojects.regexptest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameCheck {

    public static final String TEXT = "Мне очень нравится Тайланд. Таиланд это то место куда бы я поехал. тайланд - мечты сбываются!";
    public static final String TEXT1 = "регулярные выражения это круто регулярные выражения это круто регулярные выражения это круто якороль Я СЕГОДНЯ ЕЛ БАНАНЫ якороль регулярные выражения это круто";

    public static void main(String[] args) {
        System.out.println(test("BACON"));
        System.out.println(test("  BACON"));
        System.out.println(test("BACON  "));
        System.out.println(test("^BACON$"));
        System.out.println(test("bacon"));
        System.out.println(testURL("trololo.com"));     //true
        System.out.println(testURL("trololo.ua "));     //false
        System.out.println(testURL("trololo.ua"));      //true
        System.out.println(testURL("trololo/ua"));      //false
        System.out.println(testURL("i love java because it is cool.ru"));      //true
        System.out.println(testURL("BACON.ru"));        //true
        System.out.println(testURL("BACON.de"));        //false
        System.out.println(TEXT.replaceAll("[Тт]а[ий]ланд", "Россия"));
        testText();
    }

    public static boolean test(String testString) {
        Pattern p = Pattern.compile("^BACON$");
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public static boolean testURL(String stringURL) {
        Pattern p = Pattern.compile(".+\\.(com|ua|ru)");
        Matcher m = p.matcher(stringURL);
        return m.matches();
    }

    public static void testText() {
        Pattern p = Pattern.compile("(якороль).+(\\1)");
        Matcher m = p.matcher(TEXT1);

        if(m.find()) {
            System.out.println(m.group());
        }
    }
}
