package ru.resprojects.reflection.test1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;

public class SimpleReflectionExample {

    public static void main(String[] args) {
        try {
            demoReflection();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private static void demoReflection() throws Exception {
        Class example = Class.forName("ru.resprojects.reflection.test1.SimpleClass");
        System.out.println(example.getName());
        int mods = example.getModifiers();
        if (Modifier.isPublic  (mods))	{
            System.out.println("public");
        }
        if (Modifier.isAbstract(mods))	{
            System.out.println("abstract");
        }
        if (Modifier.isFinal   (mods)) {
            System.out.println("final");
        }
        Class<?> alClass = ArrayList.class;
        Class superClass = alClass.getSuperclass();
        do {
            System.out.println(superClass.getSimpleName());
            superClass = superClass.getSuperclass();
        } while (superClass.getSuperclass() != null);
        Arrays.stream(alClass.getInterfaces()).forEach(System.out::println);
        Arrays.stream(alClass.getDeclaredFields()).forEach(
            field -> {
                Class<?> fld = field.getType();
                System.out.println("Class name : " + field.getName());
                System.out.println("Class type : " + fld.getName());
            }
        );
        Arrays.stream(alClass.getMethods()).forEach(
            method -> {
                System.out.println("Method name : " + method.getName());
                System.out.println("Return type : " + method.getReturnType().getName());
                Class<?>[] params = method.getParameterTypes();
                System.out.print("Parameters : ");
                for (Class<?> paramType : params) {
                    System.out.print(" " + paramType.getName());
                }
                System.out.println();
            }
        );
        SimpleClass sc = (SimpleClass) example.newInstance();
        demoReflectionField(example, sc);
        demoReflectionMethod(example, sc);
    }

    private static void demoReflectionField(Class example, SimpleClass sc) throws Exception {
        Field f = example.getDeclaredField("first");
        f.setAccessible(true);
        String test = (String) f.get(sc);
        System.out.println("Field before SET:" + sc.getFirst());
        f.set(sc, "Test");
        System.out.println("Field after SET:" + sc.getFirst());
    }

    private static void demoReflectionMethod(Class example, SimpleClass sc) throws Exception {
        Method method = example.getMethod("simple");
        String simple = (String) method.invoke(sc);
        System.out.println("Simple:" + simple);
        Class[] paramTypes = new Class[] {String.class, String.class};
        Method concat = example.getMethod("concat", paramTypes);
        String answer = (String) concat.invoke(sc, "1", "2");
        System.out.println("Concat: " + answer);
    }

}
