package ru.resprojects.multitask;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class multitasktest1 {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            try {
                String name = Thread.currentThread().getName();
                System.out.println("Foo " + name);
                TimeUnit.SECONDS.sleep(1);
                System.out.println("Bar " + name);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
           String threadName = Thread.currentThread().getName();
           System.out.println("Thread name " + threadName);
        });
        try {
            System.out.println("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        } finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<Integer> future = executorService.submit(task);
        System.out.println("Future is done? " + future.isDone());
        Integer result = null;
        try {
            result = future.get();
            executorService.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            executorService.shutdownNow();
        }
        System.out.println("Future is Done? " + future.isDone());
        System.out.println("result " + result);
        ExecutorService executorService1 = Executors.newWorkStealingPool();
        List<Callable<String>> callables = Arrays.asList(
            () -> "task1",
            () -> "task2",
            () -> "task3"
        );
        try {
            executorService1.invokeAll(callables)
                .stream()
                .map(future1 -> {
                    try {
                        return future.get();
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                })
                .forEach(System.out::println);
            executorService1.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
            executorService1.shutdownNow();
        }
        ExecutorService executorService2 = Executors.newWorkStealingPool();
        List<Callable<String>> callables1 = Arrays.asList(
            callable("task-1", 2),
            callable("task-2", 1),
            callable("task-3", 3)
        );
        try {
            String result1 = executorService2.invokeAny(callables1);
            System.out.println(result1);
            executorService2.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            executorService2.shutdownNow();
        }
    }

    static Callable<String> callable(String result, long sleepSeconds) {
        return () -> {
            TimeUnit.SECONDS.sleep(sleepSeconds);
            return result;
        };
    }
}
