package ru.resprojects.multitask;

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemApp {

    public static void main(String[] args) {

        Runnable limitedCall = new Runnable() {

            final Random random = new Random();
            final Semaphore available = new Semaphore(3);
            int count = 0;

            @Override
            public void run() {
                int time = random.nextInt(15);
                int num = count++;

                try {
                    available.acquire();
                    System.out.println("Executing "
                        + "long-running action for "
                        + time + " seconds... #" + num);
                    TimeUnit.MILLISECONDS.sleep(time * 1000);
                    System.out.println("Done with #"
                        + num + "!");
                    available.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        for (int i = 0; i < 10; i++) {
            new Thread(limitedCall).start();
        }
    }

}
