package ru.resprojects.rmi.test2;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PrimeChecker extends Remote {
    boolean check(BigDecimal number) throws RemoteException;
}
