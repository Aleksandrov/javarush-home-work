package ru.resprojects.rmi.test2;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientRegister extends Remote {
    void register(PrimeChecker checker) throws RemoteException;
}
