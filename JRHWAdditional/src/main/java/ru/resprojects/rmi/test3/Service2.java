package ru.resprojects.rmi.test3;

public class Service2 implements Service {

    @Override
    public String getName() {
        return "Service2";
    }

    @Override
    public void execute() {
        System.out.println("Executiong service2");
    }
}
