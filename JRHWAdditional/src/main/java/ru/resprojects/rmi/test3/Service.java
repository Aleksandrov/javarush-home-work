package ru.resprojects.rmi.test3;

public interface Service {
    public String getName();
    public void execute();
}
