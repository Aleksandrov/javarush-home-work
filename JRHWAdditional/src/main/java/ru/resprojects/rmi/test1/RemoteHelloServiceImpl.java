package ru.resprojects.rmi.test1;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.TimeUnit;

public class RemoteHelloServiceImpl implements RemoteHelloService {

    public static final String BINDING_NAME = "sample/HelloService";

    public static void main(String[] args) throws Exception {
        System.out.print("Starting remote...");
        final Registry registry = LocateRegistry.createRegistry(2099);
        System.out.println(" OK");
        final RemoteHelloService service = new RemoteHelloServiceImpl();
        Remote stub = UnicastRemoteObject.exportObject(service, 0);
        System.out.print("Binding service...");
        registry.bind(BINDING_NAME, stub);
        System.out.println(" OK");
        while (true) {
            TimeUnit.MILLISECONDS.sleep(Integer.MAX_VALUE);
        }
    }

    @Override
    public Object sayHello(String name) throws RemoteException {
        String str = "Hello, " + name + "! It is " + System.currentTimeMillis() + " ms now";
        try {
            System.out.println(name + " from " + UnicastRemoteObject.getClientHost());
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
        if ("Kill".equals(name)) {
            System.out.println("Shutting down...");
            System.exit(1);
        }
        return str;
    }
}
