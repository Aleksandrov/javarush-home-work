package ru.resprojects.rmi.test1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteHelloService extends Remote {

    Object sayHello(String name) throws RemoteException;

}
